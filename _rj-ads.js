// Leader board ad

const leader = document.getElementById("leader");
if (leader) {
  (function (d, el) {
    el = d.createElement("div");
    el.style.width = "728px";
    el.style.height = "90px";
    el.style.margin = "20px auto";
    el.style.backgroundColor = "lightgrey";
    leader.appendChild(el);
  })(document);
}

// Sidebar ad

const sideTower = document.getElementById("sideTower");
if (sideTower) {
  (function (d, el) {
    el = d.createElement("div");
    el.style.width = "300px";
    el.style.height = "600px";
    el.style.margin = "20px auto";
    el.style.backgroundColor = "lightgrey";
    sideTower.appendChild(el);
  })(document);
}

// footer ad

const mainContentAd = document.getElementById("main-content");
const courseOverviewAd = document.getElementById("courseOverview");
if (mainContentAd || courseOverviewAd) {
  (function (d, el) {
    el = d.createElement("div");
    el.style.width = "970px";
    el.style.height = "250px";
    el.style.margin = "50px auto";
    el.style.backgroundColor = "lightgrey";
    if (mainContentAd) {
      mainContentAd.appendChild(el);
    }
    if(courseOverviewAd){
        courseOverviewAd.appendChild(el);
    }
  })(document);
}

// bottom sticky

(function (d, el) {
  el = d.createElement("div");
  el.style.width = "728px";
  el.style.height = "90px";
  el.style.margin = "0px auto";
  el.style.position = "fixed";
  el.style.left = "0";
  el.style.right = "0";
  el.style.bottom = "0";
  el.style.backgroundColor = "lightgrey";
  d.body.appendChild(el);
})(document);
