const courseOverview = [
  {
    course: "html",
    about:
      "<strong><u>About</u></strong><br><br> HTML is the standard markup language for creating web pages. It describes the structure of a Web Page with a series of elements, that tell the browser how to display the content.",
    objective: "<strong><u>Objective</u></strong><br><br> How to structure Web Pages. Basic Web Pages. Advanced Web Pages",
    content: "<strong><u>Content</u></strong><br><br> HTML Basics, HTML Tables, HTML Forms, HTML Media, HTML Gprahics (SVG / Canvas), HTML APIs, HTML References",
    output: "<strong><u>WINS</u></strong><br><br> Structuring Basic to Advanced Web Pages like professionals"
  },
  {
    course: "css",
    about: "<strong><u>About</u></strong><br><br>CSS stands for Cascading Style Sheet, used to style structured web pages. It can control th layout of multiple web pages all at once.",
    objective: "<strong><u>Objective</u></strong><br><br>How to layout web page? Basic to Advanced. How to create Responsive web page?",
    content: "<strong><u>Content</u></strong><br><br>CSS Basics, CSS Advanced, CSS Reposnsive, CSS Media Query, CSS Grid, CSS References",
    output: "<strong><u>WINS</u></strong><br><br>Layout Basic web page. Layout Advanced Web Page. Layout Responsive Web Page, Mobile Friendly web pages like professionals.",
  },
  {
    course: "bootstrap",
    about: "<strong><u>About</u></strong><br><br>BootStrap 5 is the newest verions of Bootstrap after BS3/BS4. It is the most popular HTML, CSS, and JavaScript framework for creating responsive, mobile-first websites.",
    objective: "<strong><u>Objective</u></strong><br><br>Undestanding predefined classes of Bootstrap to layout Responsive web pages.",
    content: "<strong><u>Content</u></strong><br><br>BS5 Dropdown, Collapse, BS5 Spinner, BS5 Forms, BS5 Grids",
    output: "<strong><u>WINS</u></strong><br><br>Being able to use pre-defined classes. Layout mobile-friendly Responsive web pages."
  },
  {
    course: "javascript",
    about: "<strong><u>About</u></strong><br><br>JavaScript is the world\'s most popular programming language of the web. It controls the behavior of the web pages",
    objective: "<strong><u>Objective</u></strong><br><br>Learn JavaScript from basic to Advanced. Mastering, how to control the behavior of web pages.",
    content: "<strong><u>Content</u></strong><br><br>JS Syntax, Keywords, JS Functions, JS Strings, JS Arrays, JS Objects, JS Classes, JS Loop, JS Classes, Async JavaScript, DOM Maniplations",
    output: "<strong><u>WINS</u></strong><br><br>Being able to create a Single page website. Understanding Functions, Arrays, Objects, Classes "
  },
  {
    course: "jquery",
    about: "<strong><u>About</u></strong><br><br>jQuery is a JavaScript library. The purpose is to make it much easier to use JavaScript on your website. 'write less, do more'.",
    objective: "<strong><u>Objective</u></strong><br><br>How to write a multi-line code in a single line using jQuery.",
    content: "<strong><u>Content</u></strong><br><br>jQuery Basics, jQuery Effects(Hide/Show, Slide, Fade, chaining), jQuery HTML, Traversing, AJAX, References",
    output: "<strong><u>WINS</u></strong><br><br>Being able to save time and write JavaScript codes quickly."
  },
  {
    course: "reactjs",
    about: "<strong><u>About</u></strong><br><br>React is a JavaScript library for building user Interfaces that is used to build single-page applications. It allows us to create reusable UI components. It is created by Facebook.",
    objective: "<strong><u>Objective</u></strong><br><br>Create a single page application like facebook",
    content: "<strong><u>Content</u></strong><br><br>ES6, Class, Components, JSX, Routers, Props, Hooks, useState, useEffect, useContext, useReducer, useRef",
    output: "<strong><u>WINS</u></strong><br><br>Being able to create a single-page app. "
  },
];

const overview = document.getElementById("courseOverview");

const courseContent = (i) => {
  (function (el) {
    el = document.createElement("h1");
    el.innerHTML = courseOverview[i].course.toUpperCase();
    overview.appendChild(el);
  })();
  const p = (content, el) => {
    el = document.createElement("p");
    el.innerHTML = content;
    overview.appendChild(el);
  };
  p(courseOverview[i].about);
  p(courseOverview[i].objective);
  p(courseOverview[i].content);
  p(courseOverview[i].output);
};

if (courseName === "html") {
  courseContent(0);
} else if (courseName === "css") {
  courseContent(1);
} else if (courseName === "bootstrap") {
  courseContent(2);
} else if (courseName === "javascript") {
  courseContent(3);
} else if (courseName === "jquery") {
  courseContent(4);
} else if (courseName === "reactjs") {
  courseContent(5);
}
