const navbar = document.getElementById("courseNavbar");
const content = document.createElement("div");
content.style.overflowY = "scroll";
content.style.width = "250px";
content.style.height = "calc(100vh - 120px)";
content.style.paddingBottom = "50px";
content.style.position = 'fixed';
content.style.backgroundColor = "rgb(231,233,235)";
navbar.appendChild(content);

topics.forEach((t) => {
  const a = document.createElement("a");
  a.innerHTML = t.topic;
  a.id = t.id;
  a.href = t.url + "#" + t.id;
  a.style.display = "block";
  a.style.padding = "7px";
  a.style.paddingLeft = '20px';
  a.style.fontSize = "18px";
  a.style.textDecoration = "none";
  a.style.color = "black";
  content.appendChild(a);
});
