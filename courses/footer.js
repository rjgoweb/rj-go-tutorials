(function (d, el) {
  const courseFooter = document.getElementById("course-footer");
  el = d.createElement("div");
  el.id = "course-footer-col1";
  el.classList = "footer-col";
  courseFooter.appendChild(el);
})(document);
(function (d, el) {
  const courseFooter = document.getElementById("course-footer-col1");
  el = d.createElement("ul");
  el.id = "footer-list-col1";
  el.classList = "footer-list";
  courseFooter.appendChild(el);
})(document);
(function (d, el) {
  const courseFooter = document.getElementById("course-footer");
  el = d.createElement("div");
  el.id = "course-footer-col2";
  el.classList = "footer-col";
  courseFooter.appendChild(el);
})(document);
(function (d, el) {
  const courseFooter = document.getElementById("course-footer-col2");
  el = d.createElement("ul");
  el.id = "footer-list-col2";
  el.classList = "footer-list";
  courseFooter.appendChild(el);
})(document);
(function (d, el) {
  const courseFooter = document.getElementById("course-footer");
  el = d.createElement("div");
  el.id = "course-footer-col3";
  el.classList = "footer-col";
  courseFooter.appendChild(el);
})(document);
(function (d, el) {
  const courseFooter = document.getElementById("course-footer-col3");
  el = d.createElement("ul");
  el.id = "footer-list-col3";
  el.classList = "footer-list";
  courseFooter.appendChild(el);
})(document);

const list1 = [
  {
    item: "HTML",
    url: "/courses/html/",
  },
  {
    item: "CSS",
    url: "/courses/css/",
  },
  {
    item: "Bootstrap",
    url: "/courses/bootstrap/",
  },
  {
    item: "JavaScript",
    url: "/courses/javascript/",
  },
  {
    item: "jQuery",
    url: "/courses/jquery/",
  },
  {
    item: "ReactJS",
    url: "/courses/reactjs/",
  },
];

const list2 = [
    {
      item: "Ref HTML",
      url: "",
    },
    {
      item: "Ref CSS",
      url: "",
    },
    {
      item: "Ref Bootstrap",
      url: "",
    },
    {
      item: "Ref JavaScript",
      url: "",
    },
    {
      item: "Ref jQuery",
      url: "",
    },
];

const details = [
    {
        item: 'Name:',
        value: 'RJgoTut'
    },
    {
        item: 'Add:',
        value: 'C-906 Charms Castle, RNE, Ghaziabad 201017'
    },
    {
        item: 'Email:',
        value: 'rjgoweb@gmail.com'
    },
    {
        item: 'Contact:',
        value: '+91-931564XXXX'
    },
    {
        item: 'whatsapp:',
        value: '+91-931564XXXX'
    },
]

for (let i = 0; i < list1.length; i++) {
  const courseFooter = document.getElementById("footer-list-col1");
  el = document.createElement("li");
  el.innerHTML =
    "<a href=" + list1[i].url + " target=_blank'>" + list1[i].item + "</a>";
  el.classList = "footer-list-item";
  courseFooter.appendChild(el);
}

for (let i = 0; i < list2.length; i++) {
  const courseFooter = document.getElementById("footer-list-col2");
  el = document.createElement("li");
  el.innerHTML =
    "<a href=" + list2[i].url + " target=_blank'>" + list2[i].item + "</a>";
  el.classList = "footer-list-item";
  courseFooter.appendChild(el);
}

for (let i = 0; i < details.length; i++) {
    const courseFooter = document.getElementById("footer-list-col3");
    el = document.createElement("li");
    el.innerHTML = details[i].item + "<br>" + details[i].value;
    el.classList = "footer-list-item";
    courseFooter.appendChild(el);
  }
  