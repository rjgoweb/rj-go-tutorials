const header = document.getElementById("course-header");

const nav = document.createElement("nav");
header.appendChild(nav);

const courses = [
    { course: "HTML", url: "/courses/html/" },
    { course: "CSS", url: "/courses/css/" },
    { course: "JavaScript", url: "/courses/javaScript/" },
    { course: "jQuery", url: "/courses/jquery/" },
    { course: "Bootstrap", url: "/courses/bootstrap/" },
    { course: "ReactJS", url: "/courses/reactjs/" }
];

window.rj_path = window.location.pathname;
window.rj_course = rj_path.split('/');
courses.forEach((c) => {
    const a = document.createElement("a");
    let isTrue = false;
    if (window.rj_course[1] === "courses") {
        isTrue = c.course.toLowerCase() === rj_course[2].toLowerCase() ? true : false;
        window.courseName = window.courseName || rj_course[2].toLowerCase();
    }
    if (isTrue) {
        a.style.padding = "24px 30px";
        a.style.backgroundColor = "rgb(4,170,109)";
        a.style.textDecoration = "underline";
    }
    a.innerHTML = c.course;
    a.href = c.url;
    nav.appendChild(a);
});