(function (v, d, o, ai) {
  ai = document.createElement("iframe");
  ai.id = "3d-vdo-cube";
  ai.width = "300";
  ai.height = "250";
  ai.style.border = "none";
  d.getElementById(o).appendChild(ai);
})(window, document, "3d-zoya-sq");

const iframe = document.getElementById("3d-vdo-cube");
const w_iframe = iframe.contentWindow;
const d_iframe = iframe.contentDocument;

(function (window, document) {
  const vdo_analyticsID = "UA-113932176-30";
  ((v, d, o, ai) => {
    ai = d.createElement("script");
    ai.async = true;
    ai.src = o;
    d.head.appendChild(ai);
  })(
    window,
    document,
    "https://www.googletagmanager.com/gtag/js?id=" + vdo_analyticsID
  );

  const vdo_analytics_Args = [
    "js",
    new Date(),
    "event",
    "loaded",
    {
      send_to: vdo_analyticsID,
      value: 1,
      event_category: "vdoaijs",
    },
  ];

  const vdo_analytics = () => {
    window.dataLayer.push(...vdo_analytics_Args);
  };

  (() => {
    window.dataLayer = window.dataLayer || [];
    vdo_analytics();
  })();

  const linkEl = (v, d, o) => {
    const ai = d.createElement("link");
    ai.rel = v;
    ai.href = o;
    d.head.appendChild(ai);
  };

  const styleEl = document.createElement("style");
  styleEl.innerHTML =
    "*{box-sizing:border-box;transition:0.3s}.scene{perspective:800px;display:flex;align-items:center;justify-content:center;height:100%;width:100%;background-size:100% 100%;background-position:center;background-repeat:no-repeat;transform-style:preserve-3d;transition:transform .3s;transform:scale(.85);z-index:99999}.scene.full-hold{animation-play-state:paused;transform:scale(1.05);z-index:999999;padding-right:100px}.scene:hover .side{animation-play-state:paused}.cube{transform-style:preserve-3d;position:relative;width:225px;height:200px;transform-origin:center center;transition:transform 2.5s ease}.side{position:absolute;width:225px;height:200px;background-color:#fff;background-size:225px 200px;background-repeat:no-repeat;background-position:center}.back{transform:translateZ(-112px) rotateX(180deg) rotateZ(180deg)}.left{transform:translateX(-112px) rotateY(270deg);}.right{transform:translateX(112px) rotateY(90deg);}.top{transform:translateY(-100px) rotateX(90deg);height:200px}.bottom{transform:translateY(75px) rotateX(270deg);height:200px}.front{transform:translateZ(112px);}@-webkit-keyframes rotate{0%{transform:rotateY(0)}25%{transform:rotateY(90deg)}50%{transform:rotateY(180deg)}75%{transform:rotateY(270deg)}100%{transform:rotateY(360deg)}}@keyframes rotate{0%{transform:rotateY(0)}25%{transform:rotateY(90deg)}50%{transform:rotateY(180deg)}75%{transform:rotateY(270deg)}100%{transform:rotateY(360deg)}}.scene-cross-btn{width:24px;height:24px;position:absolute;right:38%;top:17%;display:none;cursor:pointer}.full-hold .scene-cross-btn{display:block!important}@media screen and (max-width:650px){.scene.full-hold{padding-right:0}.scene-cross-btn{right:20px}}.rating_inner_container{position:absolute;bottom:5px;left:0}.star_bg{border-radius:0 25px 25px 0;background:#f3f3f3;padding:5px;width:105px;height:25px}.not_checked{color:gray}.checked{color:hsl(44deg 97% 49%)}.like_button{position:absolute;top:5px;right:5px}.fa{text-shadow:2px 2px 4px gray;font-size:15px}.vdo-vjs-custom-skin.vjs-user-active .vjs-tech,.vdo-vjs-custom-skin.vjs-paused .vjs-tech{filter:brightness(1)!important}.vjs-play-control,.vjs-fullscreen-control{display:none!important}";
  document.head.appendChild(styleEl);

  const logoEl = document.createElement("style");
  logoEl.innerHTML =
    "#vdo_logo{background:#fff!important;position:absolute;padding:2px 10px;right:0;bottom:12px!important;width:65px!important;z-index:99999;border-top-left-radius:8px;border-bottom-left-radius:8px;transition:bottom .4s ease-in-out;height:15px!important;font-size:10px;line-height:11px!important}#vdo_logo img{margin:0!important;box-shadow:none!important;border-radius:0!important;padding:0!important;width:100%!important;height:11px!important;object-fit:unset!important;border:none!important}";
  document.head.appendChild(logoEl);

  const contentEl = document.createElement("div");
  contentEl.innerHTML = `<div class="scene">
    <div class="cube">
       <div class="side back">
          <div id="vdo_ai_div" style="display:flex;">
             <video width="225px" height="200px" autoplay muted loop webkit-playsinline playsinline style="background-color: black;">
                <source src="https://h.vdo.ai/videos/sample/zoya.mp4">
             </video>
             <a href="https://vdo.ai/?utm_medium=video&amp;utm_term=vdo_test.net&amp;utm_source=vdoai_logo" target="_blank" id="vdo_logo" class="vdo_logo"><img src="//a.vdo.ai/core/assets/img/logo.svg" alt="VDO.AI" style="vertical-align:middle;height:11px"></a>
          </div>
          <style>.vjs-play-control,.vjs-fullscreen-control{display:none !important}</style>
       </div>
       <a target="_blank" class="side left" href="">
       </a>
       <a target="_blank" class="side right" href="">
       </a>
       <a target="_blank" class="side front" href="">
       </a>
    </div>
    <div class="scene-cross-btn">
       <svg xmlns="https://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
          <path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z" />
       </svg>
    </div>
 </div>`;
  document.body.appendChild(contentEl);

  const getBrandAds = () => {
    const brand_name = "zoya";
    const size = "300x250";
    const cube = document.getElementsByClassName("side");
    if (brand_name && size && cube) {
      for (let index = 0; index < cube.length; index++) {
        if (index == 0 && location.href.indexOf(size) == -1) {
          cube[index].children[0].src =
            "https://h.vdo.ai/videos/sample/" + brand_name + ".mp4";
          continue;
        }

        if (cube[index].children[0] && cube[index].children[0].src) {
          // cube[index].children[0].src = "https://vdo.ai/advertiser_gallery/img/" + brand_name + "/" + size + "/"+(index+1)+".jpg";
          cube[index].children[0].src =
            "https://vdo.ai/sample/3d-cubes/" +
            brand_name +
            "/img/" +
            size +
            "/" +
            (index + 1) +
            ".jpg";
        } else {
          // cube[index].style.backgroundImage = "url(https://vdo.ai/advertiser_gallery/img/" + brand_name + "/" + size + "/"+(index+1)+".jpg)";
          cube[index].style.backgroundImage =
            "url(https://vdo.ai/sample/3d-cubes/" +
            brand_name +
            "/img/" +
            size +
            "/" +
            (index + 1) +
            ".jpg)";
        }
      }
    }
  };

  const getParameterByName = (name, url = window.top.location.href) => {
    name = name.replace(/[\[\]]/g, "\\$&");
    const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  };
  getBrandAds();

  if (
    location.href.indexOf("parkavenue") == -1 &&
    location.href.indexOf("instream") == -1
  ) {
    const style = document.createElement("style");
    style.innerHTML =
      ".rating_inner_container {display: none!important;} .like_button{display:none!important;}";
    document.body.appendChild(style);
  }

  const rotate = () => {
    timer++;
    if (timer == 200 || firstRun) {
      firstRun = false;

      if (slide >= 4) {
        slide = 0;
      }
      const transform =
        cube.style.transform &&
        parseInt(cube.style.transform.split("(")[1].split(")")[0]);
      cube.style.transform =
        rotationDimention + "(" + ((transform || 0) + 90) + "deg)";
      timer = 0;
      try {
        if (window.vdo_videojs !== undefined) {
          if (slide == videoSlide) {
            vdo_videojs.getAllPlayers()[0].play();
          } else {
            setTimeout(() => {
              vdo_videojs.getAllPlayers()[0].pause();
            }, 500);
          }
        }
      } catch (error) {}
      slide++;
    }
    if (animationRunnning) requestAnimationFrame(rotate);
  };

  const playing = false;
  let timer = 0;
  let slide = 0;
  requestAnimationFrame(rotate);
  const cube = document.querySelector(".cube");

  let animationRunnning = true;

  cube.addEventListener("mouseenter", () => (animationRunnning = false));

  cube.addEventListener("mouseleave", () => {
    animationRunnning = true;
    requestAnimationFrame(rotate);
  });
  const size = "300x250";

  let rotationDimention;
  let videoSlide;

  switch (size) {
    case "970x250":
      rotationDimention = "rotateX";
      videoSlide = 1;
      break;

    case "300x250":
      rotationDimention = "rotateY";
      videoSlide = 1;
      break;

    case "300x600":
      rotationDimention = "rotateY";
      videoSlide = 3;
      break;

    default:
      rotationDimention = "rotateY";
      videoSlide = 1;
      break;
  }

  let firstRun = true;

  if (window.location.href.indexOf("flag=inframe") == -1) {
    let parent = document.querySelector(".scene");
    if (window.innerWidth <= 650) {
      const width = top.innerWidth;
      const right = 0;
      const transform = "translateX(-13%)";
    } else {
      const width = 700;
      const right = -30;
    }
  }
})(w_iframe, d_iframe);