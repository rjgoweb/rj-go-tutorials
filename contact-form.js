const contactForm = document.getElementById("contact-form");
(function (d, el) {
  el = d.createElement("form");
  el.id = "myForm";
  contactForm.appendChild(el);
})(document);

(function (d, el) {
  el = d.createElement("input");
  el.id = "userName";
  el.placeholder = "Your Name";
  d.getElementById("myForm").appendChild(el);
})(document);

(function (d, el) {
  el = d.createElement("input");
  el.id = "userEmail";
  el.type = "email";
  el.placeholder = "@";
  d.getElementById("myForm").appendChild(el);
})(document);

(function (d, el) {
  el = d.createElement("input");
  el.id = "userPhone";
  el.placeholder = "#";
  d.getElementById("myForm").appendChild(el);
})(document);

(function (d, el) {
  el = d.createElement("textarea");
  el.id = "userMessage";
  el.placeholder = "How can we help you ?";
  d.getElementById("myForm").appendChild(el);
})(document);

(function (d, el) {
  el = d.createElement("button");
  el.id = "submit";
  el.innerHTML = "Submit";
  el.classList = "button";
  d.getElementById("myForm").appendChild(el);
})(document);
